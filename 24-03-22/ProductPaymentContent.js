import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const TAX_RATE = 0.07;

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

function ccyFormat(num) {
  return `${num.toFixed(2)}`;
}


function createRow(num,  name,  membership,duration,  amount, gst, discount, paid, pending,date,mode,detail) {
  return { num, name,  membership, duration, amount, gst, discount,paid, pending,date,mode,detail};
}

function subtotal(items) {
  return items.map(({ amount }) => amount).reduce((sum, i) => sum + i, 0);
}

function subtotal1(items) {
  return items.map(({ gst }) => gst).reduce((sum, i) => sum + i, 0);
}

function subtotal2(items) {
  return items.map(({ discount }) => discount).reduce((sum, i) => sum + i, 0);
}


function subtotal4(items) {
  return items.map(({ paid }) => paid).reduce((sum, i) => sum + i, 0);
}

function subtotal5(items) {
  return items.map(({ pending }) => pending).reduce((sum, i) => sum + i, 0);
}

const rows = [
  createRow(1, 'Aanchal Gupta',		'shoes', 2, 700, 106.75, 0,700,0,'28 Mar 2019','cash','adsff'),
  createRow(2,	'Aanchal Gupta',	'test123', 2, 112, 17.08, 0,56,56,'28 Mar 2019','cash','dfsf'),
  createRow(3,	'Aanchal Gupta'	,	'shoes',1, 350, 53.38, 0,350,0,'25 Mar 2019','cash','test'),
  createRow(4,	'Aanchal Gupta',	'shoes',  1, 500, 76.25, 0, 500,0,'-','-','-'),
];

const invoiceSubtotal = subtotal(rows);
const invoiceSubtotal1 = subtotal1(rows);
const invoiceSubtotal2 = subtotal2(rows);
const invoiceSubtotal4 = subtotal4(rows);
const invoiceSubtotal5 = subtotal5(rows);

export default function SpanningTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Member Name</TableCell>
            <TableCell>Product Name</TableCell>
            <TableCell>Quantity</TableCell>
            <TableCell>Amount</TableCell>
            <TableCell>GST TAX</TableCell>
            <TableCell>Discount</TableCell>
            <TableCell>Paid Amount</TableCell>
            <TableCell>Pending Amount</TableCell>
            <TableCell>Payment Date</TableCell>
            <TableCell>Mode of Payment</TableCell>
            <TableCell>Payment Detail</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>{row.num}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.membership}</TableCell>
              <TableCell>{row.duration}</TableCell>
              <TableCell>{ccyFormat(row.amount)}</TableCell>
              <TableCell>{ccyFormat(row.gst)}</TableCell>
              <TableCell>{ccyFormat(row.discount)}</TableCell>
              <TableCell>{ccyFormat(row.paid)}</TableCell>
              <TableCell>{ccyFormat(row.pending)}</TableCell>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.mode}</TableCell>
              <TableCell>{row.detail}</TableCell>
            </TableRow>
          ))}

          <TableRow>
            <TableCell rowSpan={7} />
            <TableCell colSpan={3}>Total</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal1)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal2)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal4)}</TableCell>
            <TableCell>{ccyFormat(invoiceSubtotal5)}</TableCell>
            <TableCell colSpan={3}/>
          </TableRow>
          
        </TableBody>
      </Table>
    </TableContainer>
  );
}