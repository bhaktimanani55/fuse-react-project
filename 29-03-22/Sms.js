import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(send, calories, fat, carbs, protein) {
  return { send, calories, fat, carbs, protein };
}

const rows = [
  createData('22 Feb 2022 , 03:37 PM', 'Abhay Bhingradia',9624099099,'Completed',' Hey Abhay Bhingradia Happy Birthday to you' ),
  createData('18 Sep 2019 , 07:44 AM', 'Mehul Desai',9825309838,'Completed','Dear Mehul Desai Your Membership is about to expire on 31st Oct 2018'),
  createData('07 May 2019 , 06:48 AM',	'Swapna Rathod',9727785703,	'Completed','Dear Swapna Rathod Your Membership is about to expire on 31st Oct 2018'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Send At</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Phone</TableCell>
            <TableCell>Response</TableCell>
            <TableCell>Message</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.send}>
              <TableCell component="th" scope="row">
                {row.send}
              </TableCell>
              <TableCell>{row.calories}</TableCell>
              <TableCell>{row.fat}</TableCell>
              <TableCell>{row.carbs}</TableCell>
              <TableCell >{row.protein}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}