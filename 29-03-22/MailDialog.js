import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import {  SelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import MenuItem from '@material-ui/core/MenuItem';
import ChatIcon from '@material-ui/icons/Chat';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs() {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <IconButton aria-label="delete" onClick={handleClickOpen}>
        <ChatIcon />
      </IconButton>
      <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}
      classes={{
        paper: 'm-24',
      }}
      fullWidth
      maxWidth="sm">
        <Toolbar className="flex w-full">
        <DialogTitle id="customized-dialog-title" onClose={handleClose} variant="subtitle1" color="inherit">
          Notification
        </DialogTitle>
        </Toolbar>
        <DialogContent dividers>
        <Typography className="mt-8 mx-24">Select type of message:</Typography>
          <div className="flex">
          <Formsy>
          <SelectFormsy
                    className="mt-8 mx-24 mb-16"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Select template</em>
                    </MenuItem>
                    <MenuItem value="birthday">USER_BIRTHDAY</MenuItem>
                    <MenuItem value="followup">USER_FOLLOWUP</MenuItem>
                    <MenuItem value="payment">USER_PAYMENT</MenuItem>
                    <MenuItem value="attendance">USER_ATTENDANCE</MenuItem>
                    <MenuItem value="package">USER_PACKAGE_EXPIRE</MenuItem>
                </SelectFormsy>
          </Formsy>
          </div>

          <Typography className="mt-8 mx-24">Select User:</Typography>
          <div className="flex">
          <TextField
                  className="mt-8 mx-24 mb-16"
                  label="User Name"
                  variant="outlined"
                  fullWidth
                />
          </div>

          <Typography className="mt-16 mx-24">SMS more than 120 character will be counted as 2 :</Typography>
          <div className='flex'>
          <TextField
            className="mt-8 mx-24 mb-16"
            variant="outlined"
            multiline
            rows={5}
            required
            fullWidth
          />
          </div>

          <FormControlLabel control={<Checkbox name="checkedC" />} label="Send SMS" />
            <FormControlLabel control={<Checkbox name="checkedC" />} label="Send Mail" />
            <FormControlLabel control={<Checkbox name="checkedC" />} label="Send Push Notification" />
          
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Send
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}