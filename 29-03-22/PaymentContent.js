import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, name,  startdate, enddate,starttime,endtime,attendees) {
  return { no, name, startdate, enddate,starttime,endtime,attendees};
}

const rows = [
  createData(1, '11 Nov 2020','CBM0000726',	'Ashutosh Patel','Membership',	'₹58999',	'cash'),
  createData(2, '11 Nov 2020','CBM0000725','sheetal shah','Membership','₹50000','cash'),
  createData(3, '03 Sep 2020','CBM0000025',	'Akash Puri','Membership','₹5000',	'cash'),
  createData(4, '02 Sep 2020','CBM0000724',	'Krisil Patel',	'Membership',	'₹25000','cash'),
  createData(5, '31 Aug 2020','CBM0000723','Test Patel',	'Membership',	'₹10000',	'cash'),
  createData(6, '19 Mar 2020','CBM0000566',	'anna strong',	'Membership','₹10000','cash'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Payment Date</TableCell>
            <TableCell>Member ID</TableCell>
            <TableCell>Member Name</TableCell>
            <TableCell>Product Type</TableCell>
            <TableCell>Total Paid</TableCell>
            <TableCell>Payment Option</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>
                {row.no}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.startdate}</TableCell>
              <TableCell>{row.enddate}</TableCell>
              <TableCell>{row.starttime}</TableCell>
              <TableCell>{row.endtime}</TableCell>
              <TableCell>{row.attendees}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}