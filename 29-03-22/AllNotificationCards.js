import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles((theme)=>({
  root: {
    maxWidth: 345,
    flexGrow: 1,
  },
  media: {
    height: 140,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'left',
    color: theme.palette.text.secondary,
  },
}));

export default function MediaCard() {
  const classes = useStyles();

  return (
      <>
      
      <Grid container spacing={3}>
        <Grid item xs={2} >
          <Paper className={classes.paper}>
      <CardActionArea >
            
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>chat_bubble</Icon>  3
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>
              Total SMS
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>

    <Grid item xs={2} >
          <Paper className={classes.paper}>
      <CardActionArea >
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>cached</Icon>  0
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>
              FollowUp SMS
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>

    <Grid item xs={2} >
          <Paper className={classes.paper}>
      <CardActionArea >
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>credit_card</Icon>   0
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>Payment SMS 
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>

    <Grid item xs={2} >
          <Paper className={classes.paper}>
      <CardActionArea>
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>cake</Icon>  1
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>
              Birthday Sms
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>

    <Grid item xs={2}>
    <Paper className={classes.paper}>
      <CardActionArea>
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>chat_bubble_outine</Icon>  2
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>
              Other SMS
          </Typography>
      </CardActionArea>
    
    </Paper>
    </Grid>

    <Grid item xs={2}>
    <Paper className={classes.paper}>
      <CardActionArea>
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>email</Icon>   5
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>
              Total Emails
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>

    <Grid item xs={2}>
    <Paper className={classes.paper}>
      <CardActionArea>
          <Typography gutterBottom variant="h5" component="h2"  align='center'>
            <Icon>cached</Icon>
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p"  align='center'>
              FollowUp Emails
          </Typography>
      </CardActionArea>
    
    </Paper>
    </Grid>

    <Grid item xs={2}>
    <Paper className={classes.paper}>
      <CardActionArea>
          <Typography gutterBottom variant="h5" component="h2"  align='center'>
            <Icon>credit_card</Icon>  0
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p"  align='center'>
              Payment Emails
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>
 
    <Grid item xs={2}>
    <Paper className={classes.paper}>
      <CardActionArea>
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>cake</Icon>  0
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>
              Birthday Emails
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>

    <Grid item xs={2}>
    <Paper className={classes.paper}>
      <CardActionArea>
          <Typography gutterBottom variant="h5" component="h2" align='center'>
            <Icon>email</Icon>   4
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p" align='center'>
              Other Emails
          </Typography>
      </CardActionArea>
    </Paper>
    </Grid>
    
    
    </Grid>
    
    </>
  );
}