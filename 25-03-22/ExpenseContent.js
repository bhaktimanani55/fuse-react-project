import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, name,desc, enable, startdate, enddate ) {
  return { no, name,desc, enable,startdate, enddate };
}

const rows = [
  createData(1, '05 Sep 2018'	,'REAL MANAGMENT SERVICE',171990,	'Admin',	'-'),
  createData(2,'04 Sep 2018','JP RAI CONSULTANCY',29700	,'Admin','Admin'),
  createData(3,'04 Sep 2018','MODAL SECURITY SERVICE',112320	,'Admin','-'),
  createData(4,'07 Aug 2018',	'REAL MANAGMENT SERVICE',197334,'Admin','-'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Date</TableCell>
            <TableCell>Type</TableCell>
            <TableCell>Amount</TableCell>
            <TableCell>Created Date</TableCell>
            <TableCell>Last Updated Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>
                {row.no}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell align='justify'>{row.desc}</TableCell>
              <TableCell>{row.enable}</TableCell>
              <TableCell>{row.startdate}</TableCell>
              <TableCell>{row.enddate}</TableCell>
              
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}