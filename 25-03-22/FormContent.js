import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(no, name,desc, enable, startdate, enddate ) {
  return { no, name,desc, enable,startdate, enddate };
}

const rows = [
  createData(1, 'Health Assessment','This is a confidential health assessment questionnaire which is designed to provide insight into your health, family history and lifestyle. The following questions will assist me in providing you with the best possible care and in understanding the factors that may be playing a role in your health and fitness.The questionnaire is not designed to give a medical diagnosis. It will identify the current strengths of your health, and any risk factors that might be present.This questionnaire will take few minutes to complete. ','Yes', '2018-09-24', '2019-12-17'),
  createData(2, '24 Hour Dietary Recall','This is a confidential health assessment questionnaire which is designed to provide insight into your health, family history and lifestyle. The following questions will assist me in providing you with the best possible care and in understanding the factors that may be playing a role in your health and fitness.The questionnaire is not designed to give a medical diagnosis. It will identify the current strengths of your health, and any risk factors that might be present.This questionnaire will take few minutes to complete. .', 'Yes', '2018-09-24','2018-09-24'),
  createData(3, 'Fitness Assessment','This is a confidential health assessment questionnaire which is designed to provide insight into your health, family history and lifestyle. The following questions will assist me in providing you with the best possible care and in understanding the factors that may be playing a role in your health and fitness.The questionnaire is not designed to give a medical diagnosis. It will identify the current strengths of your health, and any risk factors that might be present.This questionnaire will take few minutes to complete. ', 'Yes','2018-09-24', '2018-09-24'),
  createData(4, '	Health Assessment','This is a confidential health assessment questionnaire which is designed to provide insight into your health, family history and lifestyle. The following questions will assist me in providing you with the best possible care and in understanding the factors that may be playing a role in your health and fitness.The questionnaire is not designed to give a medical diagnosis. It will identify the current strengths of your health, and any risk factors that might be present.This questionnaire will take few minutes to complete. ', 'Yes', '2018-11-03', '2018-11-03'),
];

export default function BasicTable() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell>Form Name</TableCell>
            <TableCell align='center'>Form Description</TableCell>
            <TableCell>Form Enable</TableCell>
            <TableCell>Created Date</TableCell>
            <TableCell>Updated Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow>
              <TableCell>
                {row.no}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell align='justify'>{row.desc}</TableCell>
              <TableCell>{row.enable}</TableCell>
              <TableCell>{row.startdate}</TableCell>
              <TableCell>{row.enddate}</TableCell>
              
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}