import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {  SelectFormsy, FuseChipSelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import MenuItem from '@material-ui/core/MenuItem';
import InventoryDialog from '../InventoryDialog';
import SupplierDialog from '../supplier/SupplierDialog';


const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        New Purchase
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Add Purchase
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>

        <div className="p-24  flex flex-row-reverse">
        <InventoryDialog/>
        <SupplierDialog />
        </div>

        <Typography className="mx-24 mt-32">Purchase Date*</Typography>
        <div className="flex">
        <TextField
          className="mb-24 mx-24 mt-8"
          variant="outlined"
          type="date"
          required
          fullWidth
        />
        </div>

        <div className='flex -mx-4'>
        <Formsy>
        <SelectFormsy
                    className="mb-24 mx-24"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Select Supplier</em>
                    </MenuItem>
                    <MenuItem value="lim">NP Lim</MenuItem>
                    <MenuItem value="phil">Phil</MenuItem>
                    <MenuItem value="muscles">Big Muscles</MenuItem>
                </SelectFormsy> 

                <SelectFormsy
                    className="mb-24 mx-24"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Select Product</em>
                    </MenuItem>
                    <MenuItem value="shoes">Shoes</MenuItem>
                    <MenuItem value="test123">test123</MenuItem>
                </SelectFormsy> 
        </Formsy>
        </div>
      
        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24"
          label="Product Cost"
          variant="outlined"
          required
          fullWidth
        />

        <TextField
          className="mb-24 mx-24"
          label="Quantity"
          variant="outlined"
          required
          fullWidth
        />

        <TextField
          className="mb-24 mx-24"
          label="Total Price"
          variant="outlined"
          required
          fullWidth
        />

        <TextField
          className="mb-24 mx-24"
          label="GST On Purchase"
          variant="outlined"
          required
          fullWidth
        />
        </div>

        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24"
          label="Product Selling Price(MRP)"
          variant="outlined"
          required
          fullWidth
        />
        <TextField
          className="mb-24 mx-24"
          label="Employee Commission(%)"
          variant="outlined"
          required
          fullWidth
        />
        <TextField
          className="mb-24 mx-24"
          label="GST on Sale"
          variant="outlined"
          required
          fullWidth
        />
        </div>
      </Dialog>
    </div>
  );
}