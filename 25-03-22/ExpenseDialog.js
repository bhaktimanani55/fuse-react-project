import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {  SelectFormsy, FuseChipSelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Checkbox from '@material-ui/core/Checkbox';


const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        Add Expense
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Add Expense
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>

        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24 mt-32"
          variant="outlined"
          type="date"
          required
          fullWidth
        />
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Amount"
          variant="outlined"
          type="number"
          required
          fullWidth
        />
        </div>
      
        <div className="flex -mx-4">
        <Formsy>
        <SelectFormsy
                    className="my-8 mx-24"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Created By(Select an Employee)</em>
                    </MenuItem>
                    <MenuItem value="admin">admin</MenuItem>
                    <MenuItem value="hemant">Hemant Solanki</MenuItem>
                    <MenuItem value="falvin">Falvin Patel</MenuItem>
                    <MenuItem value="jignesh">Jignesh Soni</MenuItem>
                    <MenuItem value="mehul">Mehul Rathod</MenuItem>
                </SelectFormsy> 
            
        <SelectFormsy
                    className="my-8 mx-24"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Select Type</em>
                    </MenuItem>
                    <MenuItem value="real">Real Management Service</MenuItem>
                    <MenuItem value="jprai">JP Rai Consultancy</MenuItem>
                    <MenuItem value="modern">Modern Security Service</MenuItem>
                    <MenuItem value="Pilates">Multi Select Dropdown</MenuItem>
                    <MenuItem value="group">Group Ex Payment</MenuItem>
                    <MenuItem value="pt">P.T Payment</MenuItem>
                    <MenuItem value="zero">Zerox Bill</MenuItem>
                    <MenuItem value="air">Air Ticket</MenuItem>
                    <MenuItem value="online">Online Shopping</MenuItem>
                </SelectFormsy> 
        </Formsy>
        </div>
        <div className='flex flex-row'>
        <TextField
                  className="my-24 mx-24"
                  label="Notes"
                  variant="outlined"
                  multiline
                  rows={5}
                  fullWidth
          />
        </div>
      </Dialog>
    </div>
  );
}