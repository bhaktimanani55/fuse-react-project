import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {  SelectFormsy, FuseChipSelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';


const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        Create Event
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Add Event
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        
        <div className="flex -mx-4">
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Event Name"
          variant="outlined"
          required
          fullWidth
        />
        
        <Typography className="mb-24 mx-24 mt-32">Start Date*</Typography>
        <TextField
          className="mb-24 mx-24 mt-32"
          variant="outlined"
          type="date"
          required
          fullWidth
        />

        
        <Typography className="mb-24 mx-24 mt-32">End Date*</Typography>
        <TextField
          className="mb-24 mx-24 mt-32"
          variant="outlined"
          type="date"
          required
          fullWidth
        />
        </div>

        <div className="flex -mx-4">
        <Formsy>
        <SelectFormsy
                    className="my-24 mx-24"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Please select the event code</em>
                    </MenuItem>
                    <MenuItem value="aerobics">Aerobics</MenuItem>
                    <MenuItem value="boxing">Boxing</MenuItem>
                    <MenuItem value="karate">Karate</MenuItem>
                    <MenuItem value="Pilates">Pilates</MenuItem>
                    <MenuItem value="Yoga">Yoga</MenuItem>
                    <MenuItem value="zumba">Zumba</MenuItem>
                    <MenuItem value="hiit">HIIT</MenuItem>
                    <MenuItem value="spinning">Spinning</MenuItem>
                    <MenuItem value="crossfit">Crossfit</MenuItem>
                    <MenuItem value="training">Functional Training</MenuItem>
                    <MenuItem value="workouts">Abs Workouts</MenuItem>
                    <MenuItem value="marathon">Marathon</MenuItem>
                </SelectFormsy> 
        </Formsy>

        <Typography className="my-24 mx-24">Start Time*</Typography>
        <TextField
          className="my-24 mr-24"
          variant="outlined"
          type="time"
          required
          fullWidth
        />

        <Typography className="my-24 mx-24">End Time*</Typography>
        <TextField
          className="my-24 mr-24"
          variant="outlined"
          type="time"
          required
          fullWidth
        />
        </div>

        <div  className="flex -mx-8">
        <TextField
          className="my-24 mx-24"
          label="Packages"
          variant="outlined"
          required
          fullWidth
        />

        <TextField
          className="my-24 mx-24"
          label="Traineers"
          variant="outlined"
          required
          fullWidth
        />
        </div>

        <div  className="flex -mx-8">
        <TextField
                  className="my-24 mx-24"
                  label="Description"
                  variant="outlined"
                  multiline
                  rows={5}
                  fullWidth
          />

              <TextField
                  className="my-24 mx-24"
                  label="Instruction"
                  variant="outlined"
                  multiline
                  rows={5}
                  fullWidth
                />
        </div>

        <div  className="my-24 mx-24">
        <FormControlLabel control={<Switch />} label="Web" />
        <FormControlLabel control={<Switch />} label="App" />
        <FormControlLabel control={<Switch />} label="Send Sms To Eligible Member" />
        </div>
      </Dialog>
    </div>
  );
}