import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import {  SelectFormsy, FuseChipSelectFormsy } from '@fuse/core/formsy';
import Formsy from 'formsy-react';
import MenuItem from '@material-ui/core/MenuItem';
import PhotoCamera from '@material-ui/icons/PhotoCamera';


const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: 'none',
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function FullScreenDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button variant="contained" color="secondary" onClick={handleClickOpen}>
        New Inventory
      </Button>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Add Inventory
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              save
            </Button>
          </Toolbar>
        </AppBar>
        
        <div className="flex -mx-4">
        <Formsy>
        <SelectFormsy
                    className="mb-24 mx-24 mt-32"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Select product type</em>
                    </MenuItem>
                    <MenuItem value="vitamins">Vitamins,Minerals & Supplements</MenuItem>
                    <MenuItem value="wear">Wear</MenuItem>
                </SelectFormsy> 
        </Formsy>
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Product Name"
          variant="outlined"
          required
          fullWidth
        />
        <TextField
          className="mb-24 mx-24 mt-32"
          label="Product Code"
          variant="outlined"
          type="number"
          required
          fullWidth
        />
        </div>

        <div className='flex -mx-4'>
        <TextField
          className="my-16 mx-24 "
          label="Weight(In KG)"
          variant="outlined"
          required
          fullWidth
        />
        <TextField
          className="my-16 mx-24 "
          label="Product Unit (kg/Liter/Pair/Piece)"
          variant="outlined"
          type="number"
          required
          fullWidth
        />
        </div>

        <div className='flex -mx-4'>
        <Formsy>
        <SelectFormsy
                    className="mb-16 mx-24 mt-32"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Select Product Brand</em>
                    </MenuItem>
                    <MenuItem value="bpi">BPI Sports</MenuItem>
                    <MenuItem value="jordan">Jordan</MenuItem>
                </SelectFormsy> 
                <SelectFormsy
                    className="mb-16 mx-24 mt-32"
                    name="related-outlined"
                    label=""
                    value="none"
                    variant="outlined"
                >
                    <MenuItem value="none">
                        <em>Select product catagory</em>
                    </MenuItem>
                    <MenuItem value="care">Health & Persoanl Care</MenuItem>
                    <MenuItem value="wear">SPorts</MenuItem>
                </SelectFormsy> 
        </Formsy>

        
        </div>

        <div className='flex -mx-4'>
        <TextField
                  className="my-24 mx-24"
                  label="Form Description"
                  variant="outlined"
                  multiline
                  rows={5}
                  fullWidth
          />
        </div>
            <div className={classes.root}>
          <input
            accept="image/*"
            className={classes.input}
            id="contained-button-file"
            multiple
            type="file"
          />
          <label htmlFor="contained-button-file">
            <Button className="ml-16" variant="outlined" color="primary" component="span">
              Upload Product Image
            </Button>
          </label>
        </div>
      </Dialog>
    </div>
  );
}